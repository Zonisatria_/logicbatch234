﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tugas2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\tDiskon GrabFood 40%");
            String kodepromo = "BAKULJKTOVO";
            double totaldc, totalblj;
            Console.WriteLine();
            Console.Write("Masukkan Belanja: ");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Jarak  : ");
            double jarak = Math.Ceiling(double.Parse(Console.ReadLine()));
            int jarak1 = Convert.ToInt32(jarak);
            double diskon;
            int ongkir, totaldiskon;


            if (jarak >= 5)
            {
                ongkir = jarak1 * 1000;
            }
            else
            {
                ongkir = 5000;
            }

            if (belanja >= 30000)
            {
                Console.Write("Kode Promo : ");
                String kode = Console.ReadLine();
                if (kode.Equals(kodepromo))
                {
                    double blj = belanja;
                    totaldc = blj * 0.4;
                    totaldiskon = Convert.ToInt32(totaldc);
                    if (totaldiskon >= 30000)
                    {
                        totaldiskon = 30000;
                    }
                    totalblj = belanja - totaldiskon + ongkir;

                    Console.WriteLine();
                    Console.WriteLine("Belanja      :" + belanja);
                    Console.WriteLine("Diskon 40%   :" + totaldiskon);
                    Console.WriteLine("Ongkir       :" + ongkir);
                    Console.WriteLine();
                    Console.WriteLine("Total Belanja\T:" + totalblj);
                }
            }
            else
            {
                totaldc = 0;
                totalblj = belanja - totaldc + ongkir;
                Console.WriteLine("Belanja      :" + belanja);
                Console.WriteLine("Diskon 40%   :" + totaldc);
                Console.WriteLine("Ongkir       :" + ongkir);
                Console.WriteLine("Total Belanja:" + totalblj);
            }
            Console.ReadKey();
        }
    }
}
