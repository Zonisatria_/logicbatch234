﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latihan1
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:

            Console.WriteLine("         Soal Tanggal 03 February");
            Console.WriteLine("");
            Console.Write("Pilih soal : ");
            int pilih = int.Parse(Console.ReadLine());
            Console.WriteLine("");

            switch (pilih)
            {
                case 1:
                    pilih1();
                    break;
                case 2:
                    pilih2();
                    break;
                case 3:
                    pilih3();
                    break;
                case 4:
                    pilih4();
                    break;
                case 5:
                    pilih5();
                    break;

                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }


        //Soal 1
        public static void pilih1()
        {
            Console.Write("Masukan Angka = ");
            string batas = Console.ReadLine();
            int[] wow = batas.Split(' ').Select(int.Parse).ToArray();
            int tmp, komp;

            for (int i = 0; i < wow.Length - 1; i++)
            {
                komp = i;
                for (int j = i + 1; j < wow.Length; j++)
                {
                    if (wow[j] < wow[komp])
                    {
                        komp = j;
                    }
                }
                tmp = wow[komp];
                wow[komp] = wow[i];
                wow[i] = tmp;
            }
            Console.WriteLine("");
            Console.Write("Hasil deret : ");
            for (int k = 0; k < wow.Length; k++)
            {
                Console.Write(wow[k] + " ");
            }
            int min = wow[0] + wow[1] + wow[2];
            int max = wow[wow.Length - 1] + wow[wow.Length - 2] + wow[wow.Length - 3];
            Console.WriteLine("");
            Console.WriteLine("Total 3 angka terkecil = " + min);
            Console.WriteLine("Total 3 angka terbesar = " + max);

            Console.ReadKey();
        }


        //Soal 2
        public static void pilih2()
        {
            Console.WriteLine("1.Jarak 1= 2KM");
            Console.WriteLine("2.Jarak 2= 500M");
            Console.WriteLine("3.Jarak 3= 1.5KM");
            Console.WriteLine("4.Jarak 4= 300M");
            Console.WriteLine("======================");

            int[] arah = new int[] { 2000, 500, 1500, 300 };

            Console.WriteLine("Masukkan Tujuan: ");
            string angka = Console.ReadLine();
            int[] tujuan = angka.Split(' ').Select(int.Parse).ToArray();
            double total = 0;
            for (int i = 0; i < tujuan.Length; i++)
            {
                total += arah[tujuan[i] - 1];
            }
            Console.WriteLine("jarak tempuh\t:" + total / 1000 + "KM");
            Console.WriteLine("Bensin\t:" + Math.Ceiling(total / 2500) + " Liter");
            Console.ReadKey();
        }


        //Soal 3
        public static void pilih3()
        {
            Console.Write("Beli Pulsa   = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            int mm = 0;
            int ll = 0;

            if (batas > 10000 && batas < 30000)
            {
                int xx = batas - 10000;
                mm = xx / 1000;
            }
            if (batas >= 30000)
            {
                int zz = batas - 30000;
                ll = (zz / 1000) * 2;
                mm = 20;
            }
            int jml = mm + ll;
            //Console.WriteLine("Keterangan :" + ss + "+" + mm + "+" + ll + "= " + jml + " Point");
            Console.WriteLine("Selamat anda mendapatkan " + jml + " Point");

            Console.ReadKey();
        }


        //Soal 4
        public static void pilih4()
        {
            Console.WriteLine("   Recursive Digit Sum");
            Console.WriteLine("");
            Console.Write("Input (cnt: 153 3)= ");
            string batas = Console.ReadLine();
            //int [] split = batas.Split(' ').Select(int.Parse).ToArray();
            //char[] ab = split[0].ToString().ToArray();
            //int temp = 0;
            //int jml = 0;

            //for (int i=0; i<ab.Length;i++)
            //{
            //    temp += int.Parse(ab[i].ToString()); 
            //}
            //jml = temp * split[1];
            //if(jml>9)
            //{

            //}

            string[] wee = batas.Split(' ');
            int a = int.Parse(wee[1]);
            int zz = 0;
            int temp = 0;
            int jml = 0;

            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < wee[0].Length; j++)
                {
                    zz = (int)char.GetNumericValue(wee[0][j]);
                    temp += zz;
                }
            }
        ulang:
            if (temp >= 9)
            {
                string xx = temp.ToString();
                char[] xxx = xx.ToCharArray();

                for (int k = 0; k < xxx.Length; k++)
                {
                    jml += (int)Char.GetNumericValue(xxx[k]);
                }
                temp = jml;
                if (temp >= 10)
                {
                    jml = 0;
                    goto ulang;
                }
                else
                {
                    Console.WriteLine("Output = " + temp);
                }
            }
            else
            {
                Console.WriteLine("Output = " + temp);
            }
            Console.ReadKey();
        }

        //Soal 5
        public static void pilih5()
        {
            Console.WriteLine("Masukkan Jumlah angka ONE= ");
            int input = int.Parse(Console.ReadLine());
            string angka = "";
            int count = 0;
            for (int i = 100; i < 1000; i++)
            {
                angka = i.ToString();
            repeat:
                int[] a = new int[angka.Length];
                for (int j = 0; j < a.Length; j++)
                {
                    string temp = "";
                    temp += angka[j];
                    a[j] = int.Parse(temp) * int.Parse(temp);
                }

                int sum = 0;
                for (int k = 0; k < a.Length; k++)
                {
                    sum += a[k];
                }
                if (sum == 1)
                {
                    Console.WriteLine(i + " is the ONE number");
                    count++;
                }
                else if (sum > 9)
                {
                    angka = sum.ToString();
                    goto repeat;
                }
                if (count == input)
                {
                    break;
                }
                Console.ReadKey();
            }
        }
    }
}
