﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_5Feb
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("\t DateTime");
            Console.WriteLine();
            for (int z = 1; z <= 4; z++)
            {
                Console.WriteLine("Soal " + z);
            }
            Console.WriteLine();
            Console.Write("Pilih Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }


        public static void soal1()
        {
            Console.WriteLine();
            Console.WriteLine("\t\t Gedung Satrio Tower");
            Console.WriteLine();
            DateTime masuk = new DateTime(2019, 8, 20, 7, 50, 00);
            DateTime keluar = new DateTime(2019, 8, 20, 17, 30, 00);
            TimeSpan waktu = keluar - masuk;
            int bayarmnt = 0;
            int jam = waktu.Hours;
            int menit = waktu.Minutes;

            Console.Write("Date\t : ");
            Console.Write(masuk.ToString("dddd, dd MMMM yyyy"));
            Console.WriteLine();
            Console.Write("Masuk Parkir\t = ");
            Console.WriteLine(masuk.ToString("HH:mm"));
            Console.Write("Keluar Parkir\t = ");
            Console.WriteLine(keluar.ToString("HH:mm"));
            Console.Write("Lama Parkir\t = " + jam + " jam " + menit + " menit");
            Console.WriteLine();

            int bayar = jam * 3000;
            if (menit > 00)
            {
                bayarmnt = (menit * 3000) / 60;
            }
            int total = bayar + bayarmnt;
            Console.WriteLine("Biaya Parkir\t = Rp " + total);
            Console.ReadKey();
        }


        public static void soal2()
        {
            Console.WriteLine();
            Console.WriteLine("\t\t Mall Mangga Dua");
            Console.WriteLine();
            DateTime masuk = new DateTime(2019, 8, 20, 13, 20, 00);
            int total = 8000;
            int jam = 0;
            int menit = 0;

            Console.Write("Date\t : ");
            Console.Write(masuk.ToString("dddd, dd MMMM yyyy"));
            Console.WriteLine();
            Console.Write("Masuk Parkir\t = ");
            Console.WriteLine(masuk.ToString("HH:mm"));
            Console.WriteLine("Biaya Parkir\t = Rp " + total);

            jam = total / 3000;
            menit = ((total % 3000) * 60) / 3000;
            Console.Write("Lama Parkir\t = " + jam + " jam " + menit + " menit");
            Console.ReadKey();
        }


        public static void soal3()
        {
            Console.WriteLine();
            Console.WriteLine("\t\tPerpustakaan");
            Console.WriteLine();
            DateTime minjam = new DateTime(2019, 9, 6);
            DateTime batas = minjam.AddDays(3);
            DateTime kembali = new DateTime(2019, 10, 7);
            TimeSpan lama = kembali - minjam;
            int n = lama.Days;
            int denda = 0;

            Console.WriteLine("Tanggal Pinjam\t = " + minjam.ToString("dd MMMM yyyy"));
            Console.WriteLine("Tanggal Kembali\t = " + batas.ToString("dd MMMM yyyy"));
            Console.WriteLine("Dikembalikan\t = " + kembali.ToString("dd MMMM yyyy"));

            denda = (n - 3) * 500;
            Console.WriteLine("Denda\t = Rp " + denda);
            Console.ReadKey();
        }


        public static void soal4()
        {
            Console.WriteLine();
            Console.WriteLine("\t\t Perjalanan");
            Console.WriteLine();
            Console.WriteLine("Rute = Rumah - Langsat - Kalbe - Langsat - Rumah");
            DateTime rumah = new DateTime(2020, 5, 2, 6, 0, 0);
            DateTime langsat = new DateTime(2020, 5, 2, 7, 25, 0);
            DateTime kalbe = new DateTime(2020, 5, 2, 8, 25, 0);
            DateTime langsat2 = new DateTime(2020, 5, 2, 17, 50, 0);
            DateTime kembali = new DateTime(2020, 5, 2, 19, 17, 0);

            //total waktu yang dibutuhkan
            TimeSpan waktu = ((langsat.AddMinutes(-15)) - rumah)+(kalbe - langsat)+(langsat2 - (kalbe.AddHours(7)))+(kembali - (langsat2.AddMinutes(10)));

            //bbm yang dibutuhkan dan total jarak
            double jarak1 = 3.7;
            double jarak2 = 24.8;
            double total = jarak1 + jarak2 + (jarak1 + 2) + (jarak2 + 5);
            double liter = total/3.5;
            liter = Math.Ceiling(liter);

            Console.WriteLine("Pergi jam\t = "+ rumah.ToString("HH:mm"));
            Console.WriteLine("kembali jam\t = " + kembali.ToString("HH:mm"));
            Console.WriteLine("Total waktu yg ditempuh\t = "+waktu.Hours+" jam "+waktu.Minutes+" menit");
            Console.WriteLine("Total jarak yg ditempuh\t = "+total+" Km");
            Console.WriteLine("BBM yg dibutuhkan\t = "+liter+" Liter");
            Console.ReadKey();
        }
    }
}
