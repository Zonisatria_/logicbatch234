﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tugas3
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("\t Deret Bilangan");
            Console.WriteLine("");
            for (int z = 1; z <= 4; z++)
            {
                Console.WriteLine("Deret " + z);
            }
            Console.WriteLine("");
            Console.Write("Pilih Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                default:
                    break;
            }
            Console.WriteLine("");
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }


        public static void soal1()
        {
            Console.WriteLine();
            Console.WriteLine("\t SOS");
            Console.WriteLine();
            string kalimat = "";
            int sos = 0;
            Console.Write("Masukan Kalimat = ");
            kalimat = Console.ReadLine();
            for (int i = 0; i < kalimat.Length; i += 3)
            {
                string match = kalimat.Substring(i, 3);
                if (match != "SOS")
                {
                    sos++;
                }
            }
            Console.Write("Total sinyal SOS adalah =" + sos);
            Console.ReadKey();
        }


        public static void soal2()
        {
            Console.WriteLine();
            Console.WriteLine("\t Palindrome");
            Console.WriteLine();
            string pal = "";
            Console.Write("Masukan Sebuah Kata = ");
            string kata = Console.ReadLine();
            Console.WriteLine();
            for (int i = kata.Length - 1; i >= 0; i--)
            {
                pal += kata[i].ToString();
            }
            if (kata.Equals(pal))
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }
            Console.ReadKey();
        }


        public static void soal3()
        {
            Console.WriteLine();
            Console.WriteLine("\t Puntung Rokok");
            Console.WriteLine();
            Console.Write("Masukan jumlah puntung rokok = ");
            int puntung = int.Parse(Console.ReadLine());
            int rokok = 0;
            int sisa = 0;
            int harga = 0;
            if (puntung >= 8)
            {
                rokok = puntung / 8;
            }
            else if (puntung < 8)
            {
                rokok = 0;
            }
            sisa = puntung - (rokok * 8);
            harga = rokok * 500;
            Console.WriteLine("Berapa jumlah rokok = " + rokok);
            Console.WriteLine("Berapa sisa puntung = " + sisa);
            Console.WriteLine("Berapakah penghasilannya = " + harga);
            Console.ReadKey();
        }


        public static void soal4()
        {
            Console.WriteLine();
            Console.WriteLine("\t Baju");
            Console.WriteLine();
            int[] baju = new int[4];
            int[] celana = new int[4];
            int[] jumlah = new int[4];
            int[] selisih = new int[4];
            int x = 0;
            int z = 0;
            int penampung = 999999;
            int output = 0;
            Console.Write("jummlah uang : ");
            int jmluang = int.Parse(Console.ReadLine());
            // intialisasi baju
            for (int i = 0; i < baju.Length; i++)
            {
                int count = i + 1;
                Console.Write("harga baju " + count + ":");
                x = int.Parse(Console.ReadLine());
                baju[i] = x;
            }
            // initialisasi celana
            for (int i = 0; i < celana.Length; i++)
            {
                int count = i + 1;
                Console.Write("harga celana " + count + ":");
                z = int.Parse(Console.ReadLine());
                celana[i] = z;
            }
            Console.WriteLine();
            Console.WriteLine("Output\t:");
            // intialisasi jumlah dan selisih 
            for (int i = 0; i < jumlah.Length; i++)
            {
                jumlah[i] = baju[i] + celana[i];
                selisih[i] = jmluang - jumlah[i];
            }
            //tampilkan baju, celana, dan jumlah
            for (int i = 0; i < baju.Length; i++)
            {
                Console.Write(baju[i] + " ");
            }
            Console.WriteLine();
            //tampilkan celana
            for (int i = 0; i < celana.Length; i++)
            {
                Console.Write(celana[i] + " ");
            }
            Console.WriteLine();
            //tampilkan jumlah
            for (int i = 0; i < jumlah.Length; i++)
            {
                Console.Write(jumlah[i] + " ");
            }
            for (int i = 0; i < selisih.Length; i++)
            {
                penampung = selisih[i];
                if (penampung > selisih[i])
                {
                    penampung = selisih[i];
                    output = jumlah[i];
                }
            }
            Console.WriteLine();
            Console.WriteLine(jumlah);
            Console.ReadKey();
        }

    }
}
