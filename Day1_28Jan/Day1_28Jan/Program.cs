﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1_28Jan
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("\t Deret Bilangan");
            Console.WriteLine("");
            for (int z = 1; z <= 10; z++)
            {
                Console.WriteLine("Deret " + z);
            }
            Console.WriteLine("");
            Console.Write("Pilih Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                case 7:
                    soal7();
                    break;
                case 8:
                    soal8();
                    break;
                case 9:
                    soal9();
                    break;
                case 10:
                    soal10();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }

        public static void soal1()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 1
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int a = 1;
            for (int i = 1; i <= batas; i++)
            {
                Console.Write(a);
                Console.Write(" ");
                a = a + 2;
            }
            Console.ReadKey();
        }

        public static void soal2()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 2
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int b = 2;
            for (int i = 1; i <= batas; i++)
            {
                Console.Write(b);
                Console.Write(" ");
                b = b + 2;
            }
            Console.ReadKey();
        }

        public static void soal3()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 3
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int c = 1;
            for (int i = 1; i <= batas; i++)
            {
                Console.Write(c);
                Console.Write(" ");
                c = c + 3;
            }
            Console.ReadKey();
        }

        public static void soal4()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 4
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int d = 1;
            for (int i = 1; i <= batas; i++)
            {
                Console.Write(d);
                Console.Write(" ");
                d = d + 4;
            }
            Console.ReadKey();
        }

        public static void soal5()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 5
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int e = 1;
            for (int i = 1; i <= batas; i++)
            {
                if (i % 3 != 0)
                {
                    Console.Write(e);
                    Console.Write(" ");
                    e = e + 4;
                }
                else
                {
                    Console.Write("* ");
                }
            }
            Console.ReadKey();
        }

        public static void soal6()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 6
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int f = 1;
            for (int i = 1; i <= batas; i++)
            {
                if (i % 3 != 0)
                {
                    Console.Write(f);
                    Console.Write(" ");
                }
                else
                {
                    Console.Write("* ");
                }
                f = f + 4;
            }
            Console.ReadKey();
        }

        public static void soal7()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 7
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int g = 2;
            for (int i = 1; i <= batas; i++)
            {
                Console.Write(g);
                Console.Write(" ");
                g = g * 2;
            }
            Console.ReadKey();
        }

        public static void soal8()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 8
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int h = 3;
            for (int i = 1; i <= batas; i++)
            {
                Console.Write(h);
                Console.Write(" ");
                h = h * 3;
            }
            Console.ReadKey();
        }

        public static void soal9()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 9
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int j = 4;
            for (int i = 1; i <= batas; i++)
            {
                if (i % 3 != 0)
                {
                    Console.Write(j);
                    Console.Write(" ");
                    j = j * 4;
                }
                else
                {
                    Console.Write("* ");
                }
            }
            Console.ReadKey();
        }

        public static void soal10()
        {
            Console.Write("Masukkan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            //Soal 10
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int k = 3;
            for (int i = 1; i <= batas; i++)
            {
                if (i % 4 != 0)
                {
                    Console.Write(k);
                    Console.Write(" ");
                }
                else
                {
                    Console.Write("XXX ");
                }
                k = k * 3;
            }
            Console.ReadKey();
        }
    }
}
