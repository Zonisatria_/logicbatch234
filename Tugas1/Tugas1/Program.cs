﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tugas1
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("\t Deret Bilangan");
            Console.WriteLine("");
            for (int z = 1; z <= 3; z++)
            {
                Console.WriteLine("Deret " + z);
            }
            Console.WriteLine("");
            Console.Write("Pilih Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                default:
                    break;
            }
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }


        public static void soal1()
        {
            Console.Write("Masukan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int a = 1;
            int b = 5;
            for (int i = 1; i <= batas; i++)
            {
                if (i % 2 != 0)
                {
                    Console.Write(a + " ");
                    a = a + 1;
                }
                else
                {
                    Console.Write(b + " ");
                    b = b + 5;
                }
            }
            Console.ReadKey();
        }


         public static void soal2()
        {
            Console.Write("Masukan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Hasil Deret : ");
            int c = 1;
            int d = 5;
            for (int i = 1; i <= batas; i++)
            {
                if (i % 3 != 0)
                {
                    if (i % 5 != 0)
                    {
                        if (i % 2 != 0)
                        {
                            Console.Write(c + " ");
                            c = c + 1;
                        }
                        else
                        {
                            Console.Write(d + " ");
                            d = d + 5;
                        }
                    }
                    else
                    {
                        Console.Write("@ ");
                    }
                }
                else
                {
                    Console.Write("* ");
                }
            }
            Console.ReadKey();
         }


         public static void soal3()
         {
             Console.Write("Masukan Nilai = ");
             int batas = int.Parse(Console.ReadLine());
             Console.WriteLine("");
             Console.Write("Hasil Deret : ");
             int e = 1;
             int f = 5;
             for (int i = 1; i <= batas; i++)
             {
                 if (i != 1)
                 {
                     if (i != batas)
                     {
                         if (i != (batas + 1) / 2)
                         {
                             if (i != batas / 2 + 1)
                             {
                                 if (i % 2 != 0)
                                 {
                                     e = e + 1;
                                     Console.Write(e + " ");
                                 }
                                 else
                                 {
                                     Console.Write(f + " ");
                                     f = f + 5;
                                 }
                             }
                             else
                             {
                                 Console.Write("@ ");
                             }
                         }
                         else
                         {
                             Console.Write("@ ");
                         }
                     }
                     else
                     {
                         Console.Write("* ");
                     }
                 }
                 else
                 {
                     Console.Write("* ");
                 }
             }
             Console.ReadKey();
         }
    }
}
