﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tugas4
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("\t DateTime");
            Console.WriteLine();
            for (int z = 1; z <= 5; z++)
            {
                Console.WriteLine("Soal " + z);
            }
            Console.WriteLine();
            Console.Write("Pilih Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }


        public static void soal1()
        {
            Console.WriteLine();
            Console.WriteLine("\t Simple Array Sum");
            Console.WriteLine();
            Console.Write("Input (cth:1,2,3,4) : ");
            string batas = Console.ReadLine();
            int[] arr = batas.Split(',').Select(int.Parse).ToArray();
            int temp = 0;

            for (int j = 0; j < arr.Length; j++)
            {
                temp += arr[j];
            }
            Console.WriteLine();
            Console.WriteLine("Hasil\t = " + temp);
            Console.ReadKey();
        }


        public static void soal2()
        {
            Console.WriteLine();
            Console.WriteLine("\t Diagonal Difference");
            Console.WriteLine();
            int[,] arr = { { 11, 2, 4 }, { 4, 5, 6 }, { 10, 8, -12 } };
            int temp = 2;
            int pri = 0;
            int sec = 0;

            for (int i = 0; i < 3; i++)
            {
                pri += arr[i, i];
                sec += arr[temp, i];
                temp--;
            }
            int total = sec - pri;
            Console.WriteLine("Diagonal Pri = " + pri);
            Console.WriteLine("Diagonal Sec = " + sec);
            Console.WriteLine("Difference   = " + total);
            Console.ReadKey();
        }



        public static void soal3()
        {
            Console.WriteLine();
            Console.WriteLine("\t Plus Minus");
            Console.WriteLine();
            Console.Write("Input (cth:-2 1 3 0 -6)\t = ");
            string data = Console.ReadLine();
            int[] split = data.Split(' ').Select(int.Parse).ToArray();
            double pos = 0;
            double zer = 0;
            double neg = 0;

            for (int i = 0; i < split.Length; i++)
            {
                double a = split[i];
                if (a < 0)
                {
                    neg++;
                }
                if (a == 0)
                {
                    zer++;
                }
                if (a > 0)
                {
                    pos++;
                }
            }

            Console.WriteLine("Output\t :");
            Console.WriteLine("\t Positive " + pos + " = " +string.Format("{0:0.000000}",(pos/split.Length));
            Console.WriteLine("\t Negative " + neg + " = " +string.Format("{0:0.000000}",(neg/split.Length));
            Console.WriteLine("\t Zero     " + zer + " = " +string.Format("{0:0.000000}",(zer/split.Length));
            Console.ReadKey();
        }



        public static void soal4()
        {
            Console.WriteLine("\t Mini-Max Sum");
            Console.WriteLine();
            Console.Write("Input (cth: 1,4,5,2,7,5) = ");
            string data = Console.ReadLine();
            int[] arr = data.Split(',').Select(int.Parse).ToArray();
            int min = 0;
            int max = 0;

            for (int i = arr.Length - 1; i > 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            for (int mi = 0; mi < 4; mi++)
            {
                min += arr[mi];
            }
            for (int ma = arr.Length - 1; ma > arr.Length - 5; ma--)
            {
                max += arr[ma];
            }
            Console.WriteLine("Four Smaller = " + min);
            Console.WriteLine("Four Greater = " + max);



            Console.ReadKey();
        }



        public static void soal5()
        {
            Console.WriteLine();
            Console.WriteLine("\t Birthday Cake Candles");
            Console.WriteLine();
            Console.Write("Input Nilai\t= ");
            string data = Console.ReadLine();
            int[] arr = data.Split(' ').Select(int.Parse).ToArray();
            int ini = 0;

            Array.Sort(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == arr[arr.Length - 1])
                {
                    ini++;
                }
            }
            Console.WriteLine("Output = " + ini);
            Console.ReadKey();
        }
    }
}
