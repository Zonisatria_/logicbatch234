﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2_29Jan
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("1. Fibonaci 2");
            Console.WriteLine("2. Fibonaci 3");
            Console.WriteLine("3. Prima");
            Console.WriteLine("4. Pohon Faktor");
            Console.WriteLine();
            Console.Write("Pilih soal : ");
            int pilih = int.Parse(Console.ReadLine());

            switch (pilih)
            {
                case 1:
                    pilih1();
                    break;
                case 2:
                    pilih2();
                    break;
                case 3:
                    pilih3();
                    break;
                case 4:
                    pilih4();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }

        //Soal 1
        public static void pilih1()
        {
            Console.Write("Masukan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.Write("Fibonaci 2 : ");
            int a = 1;
            int b = 0;
            int c = 0;
            for (int i = 1; i <= batas; i++)
            {
                c = a + b;
                Console.Write(c + " ");
                a = b;
                b = c;
            }
            Console.ReadKey();
        }

        //Soal 2
        public static void pilih2()
        {
            Console.Write("Masukan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Fibonaci 3 : ");
            int d = 1;
            int e = 1;
            int f = 1;
            int g = 0;
            for (int i = 1; i <= batas; i++)
            {
                if (i > 3)
                {
                    g = d + e + f;
                    Console.Write(g + " ");
                    d = e;
                    e = f;
                    f = g;
                }
                else
                {
                    Console.Write("1 ");
                }
            }
            Console.ReadKey();
        }

        //Soal 3
        public static void pilih3()
        {
            Console.Write("Masukan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Prima : ");
            bool x = true;

            if (batas >= 2)
            {
                for (int i = 2; i <= batas; i++)
                {
                    for (int o = 2; o < i; o++)
                    {
                        if (i % o == 0)
                        {
                            x = false;
                            break;
                        }
                    }
                    if (x)
                        Console.Write(i + " ");
                    x = true;
                }
            }
            else
            {
                Console.Write("Eweuh bilangan prima");
            }
            Console.ReadKey();
        }

        //Soal 4
        public static void pilih4()
        {
            Console.Write("Masukan Nilai = ");
            int batas = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            Console.Write("Pohon Faktor : ");
            int hasil = batas;
            for (int i = 0; i <= batas; i++)
            {
                for (int j = 2; j < batas; j++)
                {
                    if (batas % j == 0)
                    {
                        Console.Write(j + " ");
                        batas /= j;
                    }
                    if (j == batas)
                    {
                        Console.Write(j + " ");
                        batas /= j;
                    }
                }
            }
            if (batas == 1 || batas == hasil)
            {
                Console.Write("1");
            }
        }
    }
}
