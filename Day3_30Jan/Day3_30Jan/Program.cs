﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3_30Jan
{
    class Program
    {
        static void Main(string[] args)
        {
            menu:
            Console.WriteLine("\t Array");
            Console.WriteLine();
            for (int z = 1; z <= 6; z++)
            {
                Console.WriteLine("Soal " + z);
            }
            Console.WriteLine();
            Console.Write("Pilih Soal = ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? y/n");
            string lagi = Console.ReadLine();
            if (lagi.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }


        public static void soal1()
        {
            Console.WriteLine();
            Console.WriteLine("Output : ");
            int a = 1;
            int[,] xxx = new int[2, 7];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 0)
                    {
                        xxx[i, j] = j;
                        Console.Write(xxx[i, j] + " ");
                    }
                    else
                    {
                        xxx[i, j] = a;
                        a *= 3;
                        Console.Write(xxx[i, j] + " ");
                    }
                }
                Console.WriteLine("");
            }
            Console.ReadKey();
        }


        public static void soal2()
        {
            Console.WriteLine("");
            Console.WriteLine("Output : ");
            int b = 1;
            int c = 0;
            int[,] xx = new int[2, 7];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 0)
                    {
                        xx[i, j] = j;
                        Console.Write(xx[i, j] + " ");
                    }
                    else if (c % 3 != 0)
                    {
                        xx[i, j] = b;
                        b *= 3;
                        Console.Write(xx[i, j] + " ");
                    }
                    else
                    {
                        xx[i, j] = b;
                        b *= 3;
                        Console.Write("-" + xx[i, j] + " ");
                    }
                    c++;
                }
                Console.WriteLine("");
            }
            Console.ReadKey();
        }


        public static void soal3()
        {
            Console.WriteLine("");
            Console.WriteLine("Output : ");
            int d = 2;
            int[,] x = new int[2, 7];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 0)
                    {
                        x[i, j] = j;
                        Console.Write(x[i, j] + " ");
                    }
                    else if (j <= 2)
                    {
                        x[i, j] = d;
                        d += 4;
                        Console.Write(x[i, j] + " ");
                    }
                    else
                    {
                        x[i, j] = d;
                        d -= 4;
                        Console.Write(x[i, j] + " ");
                    }
                }
                Console.WriteLine("");
            }
            Console.ReadKey();
        }


        public static void soal4()
        {
            Console.WriteLine("");
            Console.WriteLine("Output : ");
            int e = 1;
            int g = 5;
            int f = 0;
            int[,] xxxx = new int[2, 7];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i == 0)
                    {
                        xxxx[i, j] = j;
                        Console.Write(xxxx[i, j] + " ");
                    }
                    else if (f % 2 != 0)
                    {
                        xxxx[i, j] = e;
                        e += 1;
                        Console.Write(xxxx[i, j] + " ");
                    }
                    else
                    {
                        xxxx[i, j] = g;
                        g += 5;
                        Console.Write(xxxx[i, j] + " ");
                    }
                    f++;
                }
                Console.WriteLine("");
            }
            Console.ReadKey();
        }


        public static void soal5()
        {
            Console.Write("Masukan Kalimat : ");
            string kalimat = Console.ReadLine();
            string hasil = "";

            string[] array = kalimat.Split(' ');
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array[i].Length; j++)
                {
                    if (j == 0)
                    {
                        hasil += array[i][j];
                    }
                    else if (j == (array[i].Length) - 1)
                    {
                        hasil = hasil + array[i][j] + " ";
                    }
                    else
                    {
                        hasil += "*";
                    }
                }
            }
            Console.WriteLine("Output = " + hasil);
            Console.ReadKey();
        }


        public static void soal6()
        {
            string kalimat;
            int kata = 0;
            Console.Write("Masukan Kalimat : ");
            kalimat = Console.ReadLine();
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (char.IsUpper(kalimat[i])) kata++;
            }
            Console.WriteLine("Output = " + kata);

            Console.ReadKey();
        }
        
    }
}
